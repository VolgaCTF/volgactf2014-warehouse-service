<?
function isWritable($file) {
    if (!file_exists($file))
        return false;

    $perms = fileperms($file);

    if (is_writable($file) || ($perms & 0x0080) || ($perms & 0x0010) || ($perms & 0x0002))
        return true;
}


$errors = [];
if (file_exists(VERSION_PATH.DS.'change_ver.cpp')) {
		$errors['change_ver.cpp'] = VERSION_PATH.DS.'change_ver.cpp is still present. Remove it.';
	}

if (file_exists(APP_PATH.DS.'otp/otp.cpp')) {
		$errors['otp.cpp'] = APP_PATH.DS.'otp/otp.cpp is still present. Remove it.';
	}

if (isWritable(CORE_ROOT.DS)) {
		$errors['core directory, writable'] = 'core directory ("/") and/or files underneath it has been found to be writable. Remove all write permissions. <br/>You can do this on unix systems with: <code>chmod -R a-w '.CORE_ROOT.DS.'</code>';
	}

if (!isWritable(FILES_PATH.DS)) {
		$errors['file directory, writable'] = 'file directory ("/files/") and/or files underneath it has not been found to be writable. Change it';
	}

if (!isWritable(APP_PATH.DS.'database/')) {
		$errors['Database folder, not writable'] = APP_PATH.DS.'database/'.' is not writable. Change it';
	}



$processowner = posix_getuid();
$coreowner = fileowner(CORE_ROOT.DS);
$coreperms = fileperms(CORE_ROOT.DS);
if ($coreowner == $processowner && ($coreperms & 0x0080)) {
		$errors['core directory, user owned, writable'] = 'The core directory  was found to be owned by the same user under whom the HTTP server is running and it has write access.';
	}

if(! is_readable(VERSION_PATH.DS.'version')){
		$errors['version is not readable'] = 'version file is not readable. Change it.';
	}

if(!empty($errors)){
	foreach ($errors as $key => $value) {
		echo "<b>key</b>:$value<hr>";
	}
	die;
}