<?
session_start();
//error_reporting(0);
//error_log(0);

define('CMS_ROOT', dirname(__FILE__));
define('DS', DIRECTORY_SEPARATOR);
define('CORE_ROOT', CMS_ROOT);
define('APP_PATH', CORE_ROOT.DS.'app');
define('FILES_PATH', CORE_ROOT.DS.'files');
define('DATABASE_NAME', APP_PATH.DS.'database/users.sqlite');



define('VERSION_PATH', APP_PATH.DS.'version');
define('OTP_PATH', APP_PATH.DS.'otp');

$app = array(
	'0' => array('type' => 'image/bmp', 'title' => 'bmp', 'ext' => 'bmp'),
	'1' => array('type' => 'image/gif', 'title' => 'gif', 'ext' => 'gif'),
	'2' => array('type' => 'image/jpeg', 'title' => 'jpg', 'ext' => 'jpg'),
	'3' => array('type' => 'image/png', 'title' => 'png', 'ext' => 'png'),
	'4' => array('type' => 'text/xml', 'title' => 'XML', 'ext' => 'xml'),
	'5' => array('type' => 'application/zip', 'title' => 'zip', 'ext' => 'zip'),
	'6' => array('type' => 'application/octet-stream', 'title' => 'sqlite', 'ext' => 'sqlite'),
	'7' => array('type' => 'application/vnd.ms-excel', 'title' => 'Excel', 'ext' => 'xls'),
	);

define('APP_STATES', count($app) - 1);
define('APP_PERIOD', 300 );
define('FLAG_PERIOD', 900 );



/*check file priv:
* 
*/

include 'security.php';




//ADD LATER
include APP_PATH.DS.'functions.php';
include APP_PATH.DS.'main.php';
