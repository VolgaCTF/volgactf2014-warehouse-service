<?
	class Validator{
		public $info;

		public function checkMime($file){
			if($file['type'] !== $this->info['type'])
				return false;
			return true;
		}

		public function checkExt($file){
			$ext = @array_pop(explode('.', $file['name']));
			if($ext !== $this->info['ext'])
				return false;
			return true;
		}

		public function checkContents($file){
			
		}

		public function validate($file){
			return ( $this->checkMime($file) && $this->checkExt($file) && $this->checkContents($file) );
		}

	
	}