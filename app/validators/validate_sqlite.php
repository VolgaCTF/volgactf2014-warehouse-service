<?
	class sqliteValidator extends Validator{
		public function checkContents($file){
			try{
				$db = new SQLite3($file['tmp_name'], SQLITE3_OPEN_READONLY);
				if($db->querySingle('SELECT "valid"') == 'valid'){
					$db->close();
					return TRUE;
				}
				return FALSE;
			} catch(Exception $e){
				return FALSE;
			}
		}
	}