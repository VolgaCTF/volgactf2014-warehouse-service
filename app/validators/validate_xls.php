<?
	include APP_PATH.DS.'validators'.DS.'lib'.DS.'PHPExcel.php';

	class xlsValidator extends Validator{
		public function checkContents($file){
		
			try{
				$fileType = PHPExcel_IOFactory::identify($file['tmp_name']);
				$fileName = $file['tmp_name'];

				// Read the file
				$objReader = PHPExcel_IOFactory::createReader($fileType);
				$objPHPExcel = $objReader->load($fileName);

				unset($objPHPExcel);
				unset($objReader);
			}
			catch(Exception $e){
				return FALSE;
			}


			return TRUE;

		}
	}