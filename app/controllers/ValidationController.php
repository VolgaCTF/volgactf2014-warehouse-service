<?

	class ValidationController{

		public static function validate($file){
			$info = AppController::appStateGetInfo();
			include APP_PATH.DS.'validators'.DS.'validator.php';
			include APP_PATH.DS.'validators'.DS.'validate_'. $info['ext'] .'.php'; //'xml.php' ;//
			
			$class = $info['ext'].'Validator';
			$validator = new $class;//xmlValidator;//
			$validator->info = $info;//array('type' => 'text/xml', 'title' => 'XML', 'ext' => 'xml');//

			return $validator->validate($file);
		}
	}