<?
	class DatabaseController{
		public static $db;

		public static function init(){
			DatabaseController::$db = new SQLite3(DATABASE_NAME);
			DatabaseController::$db->busyTimeout(3000);
			DatabaseController::$db->exec('CREATE TABLE if not exists users (id INTEGER PRIMARY KEY autoincrement , email CHAR(50))');
		} 
	}