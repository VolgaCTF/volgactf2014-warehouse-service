<?
	class FileController{
		
		//init
		public static function init(){

		}

		public static function index(){
			FileController::fileView();
		}

		// upload
		public static function fileUpload(){
			if(count($_FILES)){
				$result = ValidationController::validate($_FILES['file']);
				if($result){
					move_uploaded_file($_FILES['file']['tmp_name'], FILES_PATH.DS.UserController::userEmail().DS.$_FILES['file']['name']);
					FileController::fileView();
				} else {
					renderError(array('error' => 'Validation failed =('));
				}
			}
			else
				render(APP_PATH.DS.'views/File/upload.inc', array('title' => 'Upload Page'));
		}

		// view files
		public static function fileView(){
			$email = UserController::userEmail();
			$files = glob(FILES_PATH.DS.$email.DS.'*');
			render(APP_PATH.DS.'views/File/index.inc', array('title' => 'Files Page', 'files' => $files));
		}

		// download the file
		public static function fileDownload($params){
			$file = $params[0];
			header('Content-Type: application/octet-stream');
			die(file_get_contents(FILES_PATH.DS.UserController::userEmail().DS.$file));
		}
	}