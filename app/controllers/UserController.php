<?
	class UserController{
		private static $logined;
		private static $email;
		//init
		public static function init(){
			if(!isset($_SESSION['log'])){
				UserController::$logined = FALSE ;
			}
			else{
				UserController::$email = $_SESSION['email'];
			}
		}

		//render view
		public static function index(){
			render(APP_PATH.DS.'views/User/index.inc', array('title' => 'Login Page'));
		}

		// log user in
		public static function userLogin(){
			if(!isset($_POST['email']) || !isset($_POST['otp']))
				UserController::index();

			$email = trim($_POST['email']);
			$otp = trim($_POST['otp']);

			if(RegisterController::regRegistered($email) === FALSE)
				renderError(array('error'=>'User not found'));
			
			if(UserController::userCheckOTP($email, $otp) === FALSE)
				renderError(array('error'=>'OTP is wrong'));

			$_SESSION['log'] = 'On';
			$_SESSION['email'] = $email;
			UserController::$email = $_SESSION['email'];

			
			FileController::index();


		}

		// log user out
		public static function userLogout(){
			unset($_SESSION['log']);
			unset($_SESSION['email']);

			UserController::index();
		}

		public static function userAuth(){
			return UserController::$logined;
		}

		private static function userCheckOTP($email, $otp){
			exec(OTP_PATH.DS."otp $email $otp", $out, $result);
			if(preg_match('/Fail!/i',$out[0]) || $result == 0)
				return FALSE;
			return TRUE;
		}

		public static function userEmail(){
			return UserController::$email;
		}


		

	}