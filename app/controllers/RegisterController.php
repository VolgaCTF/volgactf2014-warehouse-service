<?
	class RegisterController{
		
		//render view
		public static function index(){
			render(APP_PATH.DS.'views/Register/index.inc', array('title' => 'Registration Page'));
		}

		public static function regRegister(){
			
			if(!isset($_POST['email']))
				RegisterController::index();

			$email = $_POST['email'];

			//if already registered
			if(RegisterController::regRegistered($email))
				renderError(array('error'=>'User is already registered'));
			
			$email = trim($email);

			//validate email	
			if (!filter_var($email, FILTER_VALIDATE_EMAIL))
				renderError(array('error'=>"Wrong email format for $email"));

			$db = DatabaseController::$db;
			$stmt = $db->prepare('INSERT INTO users (email) VALUES (:email)');
			$stmt->bindValue(':email', $email, SQLITE3_TEXT);
			$result = $stmt->execute();

			mkdir(FILES_PATH.DS.$email);

			redirect('/',array('message' => 'Registered successfuly. Your RSA token will be sent to you...At once..'));

		}

		public static function regRegistered($email){
			//SQLITE3_OPEN_READONLY
			$db = DatabaseController::$db;

			$stmt = $db->prepare('SELECT id FROM users WHERE email=:email');
			$stmt->bindValue(':email', $email, SQLITE3_TEXT);
			$result = $stmt->execute();
			if($result->fetchArray() == FALSE)
				return FALSE;
			return TRUE;
		}
	}