<?
	class AppController{
		private static $state;
		public static  $app_states;
		//init
		public static function init($app_states){
			AppController::$app_states = $app_states;
			AppController::appStateChange();
			AppController::appFilesClean();
			AppController::$state = trim(file_get_contents(VERSION_PATH.DS.'version'));
		}

		// return application current state
		public static function appStateGet(){
			return AppController::$state;
		}

		public static function appStateGetInfo(){
			return AppController::$app_states[AppController::$state];
		}

		// change app state
		private static function appStateChange(){
			system(VERSION_PATH.DS."change_ver ".VERSION_PATH.DS."version ".APP_PERIOD." ".APP_STATES);
		}

		// remove files on time count
		private static function appFilesClean(){
			$folders = glob(FILES_PATH.DS.'*');
			foreach ($folders as $folder) {
				$files = glob($folder.DS.'*');
				foreach ($files as $file) {
					$time = time();
					if( $time - filemtime($file) > FLAG_PERIOD )
						unlink($file);
				}
			}
		}
	}