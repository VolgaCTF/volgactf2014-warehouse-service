<?php

	$path = isset($_GET['PAGE']) ? $_GET['PAGE'] : "FileController/fileView/";

	$set = explode('/', $path);
	for ($p = 0; $p < count($set); $p++) {
		$set[$p] = urldecode($set[$p]);
	}
	
	$dir = stripos($set[0], '_') ?  APP_PATH.DS.substr($set[0], 0, stripos($set[0], '_')).DS: APP_PATH.DS.'controllers'.DS;


	$classes = [];
	foreach (glob($dir.'*') as $controller) {
		$classes = array_merge($classes, file_get_php_classes($controller));
	}	

	$required_classes = ["AppController","DatabaseController","UserController","FileController"];
	foreach ($required_classes as $req) {
		if(!in_array($req, $classes)){
			$dir = APP_PATH.DS.'controllers'.DS;
		}
	}

	foreach (glob($dir.'*') as $controller) {
	 	include $controller;
	}
	AppController::init($app);
	UserController::init();
	DatabaseController::init();
	$controllerClass = substr($set[0], stripos($set[0], '_') ? stripos($set[0], '_') + 1 : 0);
	
	$action = (isset($set[1]) && $set[1] != NULL) ? $set[1] : 'index';
	$arguments = explode('/', $set[2]);

	if($controllerClass === 'FileController' && UserController::userAuth() === FALSE){
		UserController::index();
	}
	if(class_exists($controllerClass) && method_exists($controllerClass, $action)){
	
	if(empty($arguments))
			$controllerClass::$action();
		else
			$controllerClass::$action($arguments);
	}
	renderError(array('error' => 'Path not found.'));		
?>
