<?
	function render($page, $parameters){
		include APP_PATH.DS.'views/Common/header.inc';
		include $page;
		include APP_PATH.DS.'views/Common/footer.inc';
		exit();
	}	


	function renderError($parameters){
		$parameters['title'] = 'Error!';
		include APP_PATH.DS.'views/Common/header.inc';
		include APP_PATH.DS.'views/Error/error.inc';
		include APP_PATH.DS.'views/Common/footer.inc';
		exit();
	}

	function redirect($page, $parameters){
		$parameters['title'] = 'redirect..';
		include APP_PATH.DS.'views/Common/header.inc';
		include APP_PATH.DS.'views/Redirect/redirect.inc';
		include APP_PATH.DS.'views/Common/footer.inc';
		exit();
	}	

	function file_get_php_classes($filepath) {
		$php_code = file_get_contents($filepath);
		$classes = get_php_classes($php_code);
		return $classes;
	}

	function get_php_classes($php_code) {
		$classes = array();
		$tokens = token_get_all($php_code);
		$count = count($tokens);
		for ($i = 2; $i < $count; $i++) {
			if (   $tokens[$i - 2][0] == T_CLASS
			&& $tokens[$i - 1][0] == T_WHITESPACE
			&& $tokens[$i][0] == T_STRING) {
				$class_name = $tokens[$i][1];
				$classes[] = $class_name;
			}
		}
		return $classes;
	}