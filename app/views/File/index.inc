<div class="container">
	<div class="navbar-header">
	<button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
		<span class="sr-only">Toggle navigation</span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
	</button>
	</div>
	<nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">
	<ul class="nav navbar-nav">
		<li>
		<a href='<?=$self?>/FileController/fileView/'> Main </a>
		</li>
		<li>
		<a href='<?=$self?>/FileController/fileUpload/'> Upload </a>
		</li>
	</ul>
	<ul class="nav navbar-nav navbar-right">
		<li><a href="<?=$self?>/UserController/userLogout/">Logout</a></li>
	</ul>
	</nav>
</div>

<div class="row">
  <div class="col-md-10 centered">
	<div class="page-header">
	<h2>
		
		<?
			if(empty($parameters['files']))
				echo "No files found. <a href='$self/FileController/fileUpload/'> Let`s upload one?</a>";
			else
				echo "Your uploaded files";
		?>
	</h2>
	</div>
	<table class="table table-striped">
	<thead>
		<tr>
		<th>
			#
		</th>
		<th>
			Name
		</th>
		<th>
			Time
		</th>
		</tr>
	</thead>
	<tbody>
		<?
			$iter = 0;
			foreach ($parameters['files'] as $path) {
				$iter ++;
				$pieces = explode('/', $path);
				$file = array_pop($pieces);
				$time = date("F d Y H:i:s.", filectime($path));
				echo "
				<tr>
					<td>
						$iter
					</td>
					<td class='td_name'>
						<a href='$self/FileController/fileDownload/$file'> $file </a>
					</td>
					<td class='content'>
						$time
					</td>
				</tr>
				";
			}
		?>
	</tbody>
	</table>
  </div>
</div>