<div class="container">
	<div class="navbar-header">
	<button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
		<span class="sr-only">Toggle navigation</span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
	</button>
	</div>
	<nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">
	<ul class="nav navbar-nav">
		<li>
		<a href='<?=$self?>/FileController/fileView/'> Main </a>
		</li>
		<li>
		<a href='<?=$self?>/FileController/fileUpload/'> Upload </a>
		</li>
	</ul>
	<ul class="nav navbar-nav navbar-right">
		<li><a href="<?=$self?>/UserController/userLogout/">Logout</a></li>
	</ul>
	</nav>
</div>

<form method='POST' enctype='multipart/form-data' action='<?=$self?>/FileController/fileUpload/'  class="form-horizontal" role="form">
		<div class="form-group">
			<label for="file" class="col-sm-2 control-label">File</label>
			<div class="col-sm-9">
				<input type='file' name='file' class="form-control" size='32'><br>
			</div>
		</div>
			
		<div class="form-group">
		<div class="col-sm-offset-2 col-sm-9">
			<button type="submit" class="btn btn-default">Upload</button>
		</div>
	</div>
</form>
	

